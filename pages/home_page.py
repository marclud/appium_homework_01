from appium.webdriver.common.touch_action import TouchAction

from pages.base_page import BasePage

from appium.webdriver.common.mobileby import MobileBy

from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class HomePage(BasePage):
    equal_selector = (MobileBy.ACCESSIBILITY_ID, 'equals')
    result_selector = (MobileBy.ID, 'result_final')
    clear_selector = (MobileBy.ACCESSIBILITY_ID, 'clear')
    arrow_panel_selector = (MobileBy.ID, 'arrow')
    cos_selector = (MobileBy.ACCESSIBILITY_ID, 'cosine')
    tan_selector = (MobileBy.ACCESSIBILITY_ID, 'tangent')
    square_selector = (MobileBy.ACCESSIBILITY_ID, 'square root')
    power_selector = (MobileBy.ACCESSIBILITY_ID, 'power')

    def calculate_cos(self, val_1, val_2):
        self.open_expert_panel()
        self.driver.find_element(*self.cos_selector).click()
        self.close_expert_panel()
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(self.digit_locator(val_1))).click()
        self.driver.find_element(*self.digit_locator(val_2)).click()
        self.driver.find_element(*self.equal_selector).click()

    def calculate_tan(self, val_1, val_2):
        self.open_expert_panel()
        self.driver.find_element(*self.tan_selector).click()
        self.close_expert_panel()
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(self.digit_locator(val_1))).click()
        self.driver.find_element(*self.digit_locator(val_2)).click()
        self.driver.find_element(*self.equal_selector).click()

    def calculate_square(self, val_1):
        self.open_expert_panel()
        self.driver.find_element(*self.square_selector).click()
        self.close_expert_panel()
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(self.digit_locator(val_1))).click()
        self.driver.find_element(*self.equal_selector).click()

    def calculate_power(self, val_1, val_2):
        self.driver.find_element(*self.digit_locator(val_1)).click()
        self.open_expert_panel()
        self.driver.find_element(*self.power_selector).click()
        self.close_expert_panel()
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(self.digit_locator(val_2))).click()
        self.driver.find_element(*self.equal_selector).click()

    def get_result(self):
        WebDriverWait(self.driver, 10).until(expected_conditions.visibility_of_element_located(self.result_selector))
        result = self.driver.find_element(*self.result_selector).text
        self.driver.find_element(*self.clear_selector).click()
        return result

    def digit_locator(self, value):
        selector = (MobileBy.ID, f'digit_{value}')
        return selector

    def open_expert_panel(self):
        TouchAction(self.driver).tap(None, 1000, 1500).perform()

    def close_expert_panel(self):
        self.driver.find_element(*self.arrow_panel_selector).click()

    def wait_until_panel_is_hidden(self):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.invisibility_of_element_located(self.arrow_panel_selector))
