import os
from appium import webdriver

from pages.home_page import HomePage

# Returns abs path relative to this file and not cwd
app_name = "calculator.apk"
app_path = os.path.abspath(os.path.join(os.path.dirname(__file__), app_name))
desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '10'
desired_caps['deviceName'] = 'Android Emulator'
desired_caps['app'] = app_path

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
home_page = HomePage(driver)


def test_cos():
    home_page.wait_until_panel_is_hidden()
    home_page.calculate_cos(9, 0)
    result = home_page.get_result()
    assert result == '0'


def test_tan():
    home_page.wait_until_panel_is_hidden()
    home_page.calculate_tan(4, 5)
    result = home_page.get_result()
    assert result == '1'


def test_square():
    home_page.wait_until_panel_is_hidden()
    home_page.calculate_square(4)
    result = home_page.get_result()
    assert result == '2'


def test_power():
    home_page.wait_until_panel_is_hidden()
    home_page.calculate_power(2, 2)
    result = home_page.get_result()
    assert result == '4'
